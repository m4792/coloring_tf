const { Router, application } = require('express');
const express = require('express');
const router = express.Router()

// importar módulo de MongoDB =>  mongoose
// Una vez creado el controlador de la bd no es necesario requerir moongoose por que
// esto se hace desde otro lugar,
// const mongoose = require("mongoose");

// Se puede llamar (importar)la vble de entorno como const o como require solamente
// Al require se le da la ruta en donde esté el var.env, en este caso esta en la misma raíz por lo
// solo se deja el nombre del archivo.  <'./carpeta/archivo'>, en este caso fue 'var.env'

// require('dotenv').config({path: 'var.env'})
// En este caso creó una constante para llamar el archivo desde el config
// se instala nodemoon para poder ir actualizando los campos en el ambiente de desarrollo <npm i -D nodemon>
// Para correrlo se moifica el json de package y se borra en scripts el mensaje de errror que no se va a utilizar
// en lugar de eso se cambia por "scripts": "nodemon ."
// en lugar de ejcutar el sevidor node index, se ejecuta <npm start>  Permanece y tiene en cuenta los cambios

const conectarDB = require('./config/db');

let app = express();

//app.use("/", function(req, res){
//    res.send("Hola Mundo");
//    }
//);

app.use(router);

// conexión de BD
conectarDB();


// Para prueba de conexión base de BD 

// mongoose.connect(process.env.URL_MONGODB)
//     .then(function(){console.log("Conexión Establecida con mongoDB Atlas")})
//     .catch(function(e){console.log(e)}) 

// // modelo schema de la base de datos
// const userSchema = new mongoose.Schema({
//     id:Number,
//     nombre:String,
//     apellido:String,
//     email:String,
//     psw:String
// });

// const photoSchema  =new mongoose.Schema(
//     {
//         id: Number,
//         id_user: String,
//         url_foto_blanco_negro: String,
//         url_foto_transformada: String
//     }
// )

// var Dataset = mongoose.model('data', dataSchema, 'data');

// var User = mongoose.model('user', dataSchema, 'user');

// creación de modelo
// Para mantener el mismo nombre de la colección creada es necesario agregar despues del schema e nombre de la colección
// // const modelUser = mongoose.model('modelo_usuario', userSchema, 'users');
// const modelPhotos = mongoose.model('modelo_photo', photoSchema, 'photos');

// await modelUser.save();

// Para insertar un dato en la bd
// modelUser.create({
//     id: 2,
//     nombre: "Paquita",
//     apellido: "Vaca",
//     email: "paca@lavaca.com",
//     psw: "lavaquita"
//     },
//     // función flecha 
//     (error) => {
//         //console.log("Ocurrió el siguiente error")
//         if (error) return console.log(error)
//     }
// )

// CRUD => Read

// modelPhotos.find((error, photos) =>{
//     if (error) return console.log(error);
//     console.log(photos);
//     }
// );

// CRUD => Update

// modelUser.updateOne({id: 1}, {psw: "Pedro1234"}, (error) =>{
//     if (error) return console.log(error);
//     }
// );

// CRUD => Delete


// modelUser.deleteOne({id:2}, (error) => {
//     if (error) return console.log(error);
//     }
// );

// ********************************************************
// ************ Rutas con respecto al CRUD ****************
// ********************************************************

// A tener en cuenta
// Uso de archivos tipo json en la app

app.use(express.json());

// CORS (cross origin resource sharing) => Mecánismos o políticas de seguridad
// con protocolos http
// Hay que instalar una dependencia

const cors = require('cors');

// solicitudes al controlador CRUD
const crudUser = require('./controllers/controlUser');
// El slage corresponde a la ruta ppal

// CRUD => Create
router.post('/', crudUser.createUser);

// CRUD => Read
router.get('/', crudUser.findUser);

// Para los siguientes métodos es necesario especificarle el id en el postman
// se agrega el <:id> eejem postman: <localhost/4000/61a4b8d47829ee1f840b5fdd>

// CRUD => Update
router.put('/:id', crudUser.updateUser);

// CRUD => Delete
router.delete('/:id', crudUser.deleteUser);

// Establecer las rutas con respecto al CRUD

// app.get("/", function(req, res){
//     res.send("Estoy utilizando el médoto GET")
// });
// Función arrow o función anoniman
// app.get("/", (req, res) =>{
//     res.send("Estoy utilizando el médoto GET")
// });

// Prueba de postman para probar el post de la página sin bd
/* router.post("/metodospost", function(req, res){
    res.send("utilizando el método POST")
}); */

// Prueba de postman para probar el post con conexión de bd

// Este router es para probar la conexión, en una aplicación va con la variable de entorno
// router.post("/metodospost", function(req, res){
//     console.log("Antes de la conexión a la BD")
//     const user = 'b06g05';
//     const psw = 'b06g05123';
//     const db = "coloring_ai"; 
//     const url = `mongodb+srv://${user}:${psw}@cluster0.ltjqv.mongodb.net/${db}?retryWrites=true&w=majority`;
//     mongoose.connect(url)
//         .then(function(){console.log("Conexión Establecida con mongoDB Atlas")})
//         .catch(function(e){console.log(e)});
    
//     res.send("utilizando el método POST") 
//     console.log("Después de la conexión a la BD")
// });




// Conexión con la base de datos
// const user = 'b06g05';
// const psw = 'b06g05123';
// const db = "coloring_ai"; 
// const url = `mongodb+srv://${user}:${psw}@cluster0.ltjqv.mongodb.net/${db}?retryWrites=true&w=majority`;
            
// router.post("/metodospost", function(req, res){
//     console.log("Antes de la conexión a la BD")
// });

// mongoose.connect(url)
//     .then(function(){console.log("Conexión Establecida con mongoDB Atlas")})
//     .catch(function(e){console.log(e)}) 

app.listen(4000);

console.log("La aplicación se ejecuta en: http://localhost:4000")
