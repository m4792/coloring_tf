// Importar el modelo de la base de datos para el CRUD
const { model } = require('mongoose');
const modelUser = require('../models/modelUser');
// Se exportan en diferenrtes variables los
// métodos para el CRUD

// CRUD => Create
// para utilizar la sentencia wait que permita hacer la espera a la 
// ejecución de la función se requiere una función asincrona.
// (request, response) : Parámetros de la función
exports.createUser = async(req, res) => {
    try {

        // let instancia la variable, se instancia con respecto
        // a la ejecución del modelo.
        let user;

        user = new modelUser(
            {
                // Por el momento se especifican las rutas
                id: 2,
                nombre: "Juana",
                apellido: "Sinnombre",
                email: "juanasinnombre@uis.com.co",
                psw: "juana123"
            }
        );
        await user.save();
        // se establece la respuesta:
        res.send(user);
    } catch (error){
        console.log(error).send('Error al guardar producto');
        // Error 500: No guardó en el servidor
        res.status(500);
    }
};

// CRUD => Read
exports.findUser = async(req, res) => {
    try {
        // el tipo de find se escoge de acuerdo a lo que se quiera
        // encontrar
        const user = await modelUser.find();
        res.json(user);

    } catch (error) {
        cobnsole.log(error);
        res.status(500).send('Error al consultar los usuarios');
    }
}; 
// CRUD ==> Update
exports.updateUser = async (req, res) =>{
    try{
        const user = await modelUser.findById(req.params.id);
        if (!user){
            console.log(user);
            // se pueden enviar json, en lugar de send
            console.log(user);
            res.status(404).json({msg: 'El usuario no existe'});   
        } else {
            await modelUser.findByIdAndUpdate({_id: req.params.id}, {psw: '123'});
            res.json({msg: 'Usuario actualizado correctamente'})
        }
    } catch (error) {
        console.log(error);
        // Acá el error es con el servidor
        res.status(500).send('Error al actualizar el usuario')
    }
};

// CRUD => Delete

exports.deleteUser = async (req, res) => {
    try{
        const user = await modelUser.findById(req.params.id);
        if(!user){
            res.status(404).send('El usuario no existe');
        } else {
            // tambien sirve findByIdAndDelete
            await modelUser.findByIdAndRemove({_id: req.params.id});
        res.json({msg: 'El usuario ha sido eliminado.'});
        }
    } catch (error){
        console.log(error);
        // Acá el error es con el servidor
        res.status(500).send('Error al eliminar el usuario')

    }
};