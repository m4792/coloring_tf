import { NgModule } from '@angular/core';
// Se importa el roouter module
import { RouterModule, Routes } from '@angular/router';
// import { CommonModule } from '@angular/common';  //No se va a tener en cuenta

const routes: Routes=[
  {
    path:'home',
    loadChildren: () => import('./@public/pages/home/home.module').then(m => m.HomeModule)
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes),

    // CommonModule // no se va a tener en cuenta
  ]
})
export class AppRoutingModule { }
